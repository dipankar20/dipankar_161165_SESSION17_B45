<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();
echo "<div id='message'> $msg </div>";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Create Form</title>
</head>
<body>
<form action="store.php" method="post">
    Enter Book Name:
        <input type="text" name="bookName">
    <br/><br/>
    Enter Author Name:
        <input type="text" name="authorName">
    <br/><br/>
    <input type="submit">
</form>
</body>
</html>
